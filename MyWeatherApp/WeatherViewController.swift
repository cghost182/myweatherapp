//
//  WeatherViewController.swift
//  MyWeatherApp
//
//  Created by Christian Collazos on 9/10/19.
//  Copyright © 2019 ChristianC. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    // MARK: - OUTLETS
    
    @IBOutlet weak var providerBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var temperatureUnitBtn: UIButton!
    @IBOutlet weak var Datapicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    // MARK: - Actions

    @IBAction func providerTap(_ sender: Any) {
    }
    @IBAction func cityTap(_ sender: Any) {
    }
    @IBAction func temperatureUnitTap(_ sender: Any) {
    }
}

